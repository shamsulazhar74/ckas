import { Component } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  constructor() {}

  slideOptsTop = {
    spaceBetween: 0,
    slidesPerView: 1.35,
  };
}
